Source: libfast-perl
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Steffen Moeller <moeller@debian.org>
Section: perl
Priority: optional
Build-Depends: debhelper-compat (= 12)
Build-Depends-Indep: perl
Standards-Version: 4.4.1
Vcs-Browser: https://salsa.debian.org/med-team/libfast-perl
Vcs-Git: https://salsa.debian.org/med-team/libfast-perl.git
Homepage: https://metacpan.org/release/FAST

Package: libfast-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends}
Description: FAST Analysis of Sequences Toolbox
 The FAST Analysis of Sequences Toolbox (FAST) is a set of Unix tools (for
 example fasgrep, fascut, fashead and fastr) for sequence bioinformatics
 modeled after the Unix textutils (such as grep, cut, head, tr, etc). FAST
 workflows are designed for "inline" (serial) processing of flatfile
 biological sequence record databases per-sequence, rather than per-line,
 through Unix command pipelines. The default data exchange format is
 multifasta (specifically, a restriction of BioPerl FastA format). FAST
 tools expose the power of Perl and BioPerl for sequence analysis to
 non-programmers in an easy-to-learn command-line paradigm.
 .
 You do not need to know Perl or BioPerl to use FAST.
