Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://metacpan.org/release/FAST
Upstream-Contact: David H. Ardell, <dhard at cpan.org>
    and members of the Ardell Laboratory 
    and other contributors including:
    Travis Lawrence
    Dana Carper
    Katherine Amrine
    Kyle Kauffman
    Claudia Canales
Upstream-Name: FAST
Files-Excluded: */*.sty
                */*.aux
                */*.log
                */.#*
                */*.bak
Comment: *.sty files are all in texlive-latex-extra and there
         is no point in bluring d/copyright with code copies of
         existing files.
         Once we are removing files, we also get rid of tex logs

Files: *
Copyright: 2012-2015 David H. Ardell, <dhard at cpan.org>
    and members of the Ardell Laboratory 
    and other contributors including:
    Travis Lawrence
    Dana Carper
    Katherine Amrine
    Kyle Kauffman
    Claudia Canales
License: Artistic or GPL-1+

Files: lib/FAST/Bio/SeqIO/bsml.pm
Copyright: 2001 Charles Tilford <tilfordc@bms.com>
License: LGPL-2.1
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 On Debian systems you can find a copy of the full text of the
 GNU Lesser General Public License version 2.1 at
 /usr/share/common-licenses/LGPL-2.1 .

Files: debian/*
Copyright: 2019, Steffen Moeller <moeller@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.
